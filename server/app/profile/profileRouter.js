const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../../config/database');
const User = require('../user/user');
const multer = require('multer');
const fs = require('fs');

// --------------------------------Get Profile---------------------------------------------
router.get('', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({users: req.user});
});

//--------------------------------Update Avatar------------------------------------------
const upload = multer(config.destination);
router.post('/avatar', upload.single('avatar'),passport.authenticate('jwt', {session:false}), (req, res, next) => {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were any
  /** When using the "single" **/
  var tmp_path = req.file.path;
  /** The original name of the uploaded file in the variable "originalname". **/
  var target_path = "server\\img\\" + req.user.accountCode+ req.file.originalname ;

  console.log("**got the upload img POST",{"body":req.body,"file path": target_path});

  fs.rename(tmp_path,target_path, function(err) {
    if ( err ) {
      console.log('ERROR: ' + err);
    }
    else console.log("**upload complete");
  });

  /** Delete old avatar**/
  let oldAvatarName = req.user.avatar.toJSON();
  let oldAvatarPath = "server\\img\\"+ oldAvatarName;
  let resultHandler =  (err) => {
    if (err) {console.log("unlink failed", err);
    } else {console.log("**old file deleted");    }
  };
  if (oldAvatarName && oldAvatarName!==(req.user.accountCode+ req.file.originalname)){
    fs.unlink(oldAvatarPath, resultHandler);
  }

  /** Change avatar name in DB**/
  let newAva = {avatar:  req.user.accountCode + req.file.originalname };
  let accId = { accountCode: req.user.accountCode };
  User.updateUserProfile(accId, newAva, (err)=> {
    if(err) {
      console.log("update avatar name fail: ", err);
    } else {
      console.log("**update avatar name successful");
    }
  });
  // res.setHeader("Content-Type", "text/html");
  res.json({status: "update avatar successful"});
  res.end();
});

// -----------------------------------Update user info-------------------------------
router.put('',passport.authenticate('jwt', {session:false}), (req, res) => {

  let updatedUser = ({
    fullName: req.body.fullName,
    email: req.body.email,
    userBirthday: req.body.userBirthday,
    job: req.body.job,
    company: req.body.company,
    address: req.body.address,
    phone: req.body.phone,
    gender: req.body.gender
  });

  function clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }

  clean(updatedUser);
  console.log(updatedUser);

  let accId = { accountCode: req.user.accountCode };
  User.updateUserProfile(accId, updatedUser,(err, user) => {
    if(err){
      console.log("ERR: " + err);
      return false;
    } else {
      console.log("user profileRouter.js of " + req.user.fullName + " was saved");
    }
  });
  return res.json({success: true, msg:'User profileRouter.js updated'});
});
module.exports = router;
