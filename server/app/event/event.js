const mongoose = require('mongoose');
const relationship = require("mongoose-relationship");
const config = require('../../config/database');
const Enrollment = require('./../enrollment/enrollment');
const ObjectId = require("mongoose").Schema.Types.ObjectId;

// Event Schema
const EventSchema = new mongoose.Schema({
  title: {type: String, required: true},
  location: {type: String},
  address: {type: String, required: true},
  isPublic: {type: Boolean, default: 1},
  coverImg: { data: Buffer, contentType: String },
  start: {type: Date },
  end: {type: Date},
  description: {type: String},
  latitude: {type: Number},
  longitude: {type: Number},
  optionalChoice: {type: Boolean},
  optionalNote: {type: String},
  enrollments:[{type: ObjectId, ref: "Enrollment"}],
},{
  timestamps: true
});

const Event = module.exports = mongoose.model('Event', EventSchema);
//
// module.exports.getEventById = (id, callback) => {
//   Event.findById(id, callback);
//   let currentEvent = Event.findById(id, callback);
//   // console.log(currentEvent);
//
// };

module.exports.addEvent = (newEvent, cb) => {
  // console.log("addEvent-newEvent------------------------ ", newEvent);
        newEvent.save(cb);
};

// module.exports.updateEventPass = (accId, newPass, cb) => {
//   Event.findOneAndUpdate(accId, newPass, cb);
// };
//
// module.exports.updateEventProfile = (accId, newProfile, cb) => {
//   Event.findOneAndUpdate(accId, newProfile, cb);
// };
//
module.exports.getEventByEventId = (eventId, callback)=> {
  let query = {_id: eventId};
  console.log("eventId-getEventByEventId-------------- " + eventId);
  Event.findOne(query, callback);
};


module.exports.get3EventsForSlideshow= (cb)=> {
  Event.find({$where: 'this.enrollments.length > 0'}).sort('-updatedAt').limit(3)
      .exec(function(err,events){
        if (err) console.log(err);
        // console.log('news-load----------', events); // Do something with the array of 10 objects
        cb(events);
      })
};

// module.exports.comparePassword = (candidatePassword, hash, callback) => {
//   bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
//     if (err) throw err;
//     callback(null, isMatch);
//   });
// };
//
