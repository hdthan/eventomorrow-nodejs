const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../../config/database');
const Event = require('./event');
const User = require('./../user/user');
const Enrollment = require('./../enrollment/enrollment');
const relationship = require("mongoose-relationship");

//--------------------------------------Add/create event--------------------------------------
router.post('/create',passport.authenticate('jwt', {session:false}), (req, res) => {

  let userId = req.user._id;
  function clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }
  clean(req.body);

  let newEvent = new Event({
    title: req.body.title,
    location: req.body.location,
    address: req.body.address,
    isPublic: req.body.isPublic,
    coverImg: req.body.coverImg,
    start: req.body.start,
    end: req.body.end,
    description: req.body.description,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    optionalChoice: req.body.optionalChoice,
    optionalNote: req.body.optionalNote,
  });
  Event.addEvent(newEvent, (err, event) => {
    if(err){
      console.log("ERR: " + err);
      return false;
    } else {
      console.log("event saved------------------------------------ \n", event);
    }
  });

  let newEnroll = new Enrollment({_event: newEvent._id, _user: userId, enrollmentRole: "organizer"});
  newEnroll.save();
  return res.json({success: true, msg:'Event saved, enrollment updated',
                    createdEnrollment: newEnroll, createdEvent: newEvent});
});

//--------------------------------------Get all event--------------------------------------
router.get('/all',passport.authenticate('jwt', {session:false}), (req, res) => {
  Event.find({}, function(err, events) {
    var eventMap = {};

    events.forEach(function(event) {
      eventMap[event._id] = event;
    });
    res.json({success: true, listOfAllEventResult: eventMap});
  });
});

//--------------------------------------Get event by event id--------------------------------------
router.get('/id/:eventId', (req, res) => {
  console.log(req.params.eventId);
  Event.getEventByEventId(req.params.eventId, (err,event) => {
    if(err){
      console.log("ERR: " + err);
      return err;
    } else {
      res.json({success: true, eventResult: event});
    }
  });
});

//------------------------Get list of events by userId and their role----------------------------------
router.get('/role/:enrollmentRole',passport.authenticate('jwt', {session:false}), (req, res) => {
  Enrollment.getEventByUserIdAndRole(req.user._id, req.params.enrollmentRole, events => {
    console.log("events-getEventByUserIdAndRole---------",events);
    res.json({success: true, listEventResult: events});
  });
});

//------------------------Get list of popular events for slideshow----------------------------------
router.get('/slideshow', (req, res) => {
  Event.get3EventsForSlideshow(events => {
    console.log("events-getEventByUserIdAndRole---------",events);
    res.json({success: true, listEventResult: events});
  });
});

//--------------------------------------Get upcoming(all live and active) event--------------------------------------
router.get('/upcoming',(req, res) => {
  let today = new Date().toISOString();
  Event.find({'start':{$gte: today}, isPublic:true}, function(err, events) {
    let eventMap = {};
    events.forEach(function(event) {
      eventMap[event._id] = event;
    });
    console.log("upcoming(all live and active) event----------------------------------\n", eventMap);
    return res.json({success: true, listOfUpcomingEventResult: eventMap});
  });
});

//todo /api/event/supporter-live/size getSizeOfListLiveEventSupporterByUserId
// todo GET /api/event/supporter-live/{num} getListLiveEventSupporterByUserId
// todo GET /api/event/supporter-pass/size getSizeOfListPassEventSupporterByUserId
// todo GET /api/event/supporter-pass/{num} getListPassEventSupporterByUserId
module.exports = router;
