let mongoose = require('mongoose');
let bcrypt = require('bcryptjs');
let config = require('../../config/database');
const User = require('../user/user');
const Event = require('./../event/event');
const ObjectId = require("mongoose").Schema.Types.ObjectId;
const relationship = require("mongoose-relationship");

// Enrollment Schema
const EnrollmentSchema = new mongoose.Schema({
  enrollmentRole: {type: String,default:"participant"},
  authCode: {type: String,default:""},
  checkIn: {type: Boolean,default:0},
  checkInDate: {type: Date, expires: 60*60*24},
  confirm: {type: Boolean,default:"0"},
  enrollDate: {type: Date, expires: 60*60*24},
  emailReminder: {type: Boolean,default:1},
  optionalNote: {type: String,default:""},
  _event: {type: ObjectId, ref: "Event", childPath: "enrollments"},
  _user: {type: ObjectId, ref: "User", childPath: "enrollments"}
});
EnrollmentSchema.plugin(relationship, {relationshipPathName: ['_event','_user']});

const Enrollment = module.exports = mongoose.model('Enrollment', EnrollmentSchema);
//
module.exports.addEnrollment = (newEnrollment, cb) => {
  console.log("addEnrollment-newEnrollment::: ", newEnrollment);
  newEnrollment.save(cb);

};

// module.exports.getEnrollmentByEventId = (eventId, callback) => {
//
//   Event.findOne({ title: 'Once upon a timex.' })
//       .populate('_creator').
//       exec(function (err, story) {
//     if (err) return handleError(err);
//     console.log('The creator is %s', story._creator.name);
//     // prints "The creator is Aaron"
//   });
//
//   Enrollment.findById(_id, callback);
//   // console.log(currentEnrollment);
// //todo
// };

module.exports.getUserByEventId = (eventId, callback) => {
  console.log("eventId----------------", eventId);
  Enrollment.findOne({ _event: eventId})
      .populate('_user _event').
      exec(function (err, enrollment) {
    if (err) throw err;
    console.log("enrollment-------------", enrollment);
    console.log('The user\'s full name is %s', enrollment._user.fullName);
    // prints "The creator is Aaron"
    callback( enrollment);
  });
};


module.exports.getEventByUserIdAndRole = (userId, enrollmentRole, callback) => {
  console.log("userId-getEventByUserIdAndRole---------------", userId);
  console.log("enrollmentRole-getEventByUserIdAndRole---------------", enrollmentRole);

  Enrollment.find({ _user: userId, enrollmentRole:enrollmentRole })
    .populate('_event').exec( function(err, enrollments) {
        if (err) throw err;
        var eventMap = {};
      enrollments.forEach(function(enrollment, index) {
      eventMap[index] = enrollment._event;
    });
    callback(eventMap);
  });
};
//
  // Enrollment.findOne({ _user: userId})
  //     .populate('_user _event').
  //     exec(function (err, enrollment) {
  //   if (err) throw err;
  //   console.log("enrollment-------------", enrollment);
  //   console.log('The user\'s full name is %s', enrollment._user.fullName);
  //   // prints "The creator is Aaron"
  //   callback( enrollment);
  // });

  // Enrollment.findById(_id, callback);
  // console.log(currentEnrollment);

module.exports.addEnrollment = (userId, eventId, userRole, cb) => {
  console.log("userId-addEnrollment ", userId);
  console.log("eventId-addEnrollment ", eventId);
  let roles = [
    "admin",
    "organizer",
    "participant",
    "supporter",
  ];
  let found = roles.includes(userRole);
  if(found){
  let newEnroll = new Enrollment({_event: eventId, _user: userId, Role: userRole});
  newEnroll.save(cb);
    return newEnroll;
  } else cb("Role not found");
};

// module.exports.updateEnrollmentPass = (accId, newPass, cb) => {
//   Enrollment.findOneAndUpdate(accId, newPass, cb);
// };
//
// module.exports.updateEnrollmentProfile = (accId, newProfile, cb) => {
//   Enrollment.findOneAndUpdate(accId, newProfile, cb);
// };
//
// module.exports.getEnrollmentByAccountId = (accountCode, callback)=> {
//   let query = {accountCode: accountCode};
//   console.log("accountCode-getEnrollmentByEnrollmentname " + accountCode);
//   Enrollment.findOne(query, callback);
// };
//
// module.exports.comparePassword = (candidatePassword, hash, callback) => {
//   bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
//     if (err) throw err;
//     callback(null, isMatch);
//   });
// };

