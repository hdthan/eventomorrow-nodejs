const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../../config/database');
const Enrollment = require('./enrollment');
const Event = require('./../event/event');
const User = require('./../user/user');
const mongoose = require('mongoose'),
      relationship = require("mongoose-relationship");
const ObjectId = require("mongoose").Schema.Types.ObjectId;

//------------------------Get User By Event Id - Demo relation----------------------------------
router.post('',passport.authenticate('jwt', {session:false}), (req, res) => {
  let eventId = "5978758e95ece74670bb3dbd";

  Enrollment.getUserByEventId(eventId,(enrollment)=>{
    // if (err) throw err;
    console.log("return EN---------", enrollment);
  });
  return res.json({success: true, msg:'Enrollment saved, updated'});

  //
  //   let newEnrollment = new Enrollment({
  //     event: event._id,
  //     enUserRole: "{fffffffffff: String,default}",
  //     authCode: "than huynh",
  //     checkIn: "{fffffffffff: Boolean,default:1}",
  //     checkInDate: "",
  //     confirm: "{fffffffffff: Boolean,default:}",
  //     enrollDate: "",
  //     emailReminder: "{fffffffffff: Boolean,default:1}",
  //     optionalNote: "{fffffffffff: String,default:}",
  //   });
  // Enrollment.addEnrollment(newEnrollment, (err) => {
  //   if (err) throw err;
  // });

  // let newEvent = new Event({
  //   title:"thanhuynh123",
  //   location:"thanhuynh123",
  //   address:"thanhuynh123",
  //   isPublic:"thanhuynh123",
  //   coverImg:"thanhuynh123",
  //   createdDate:null,
  //   endDate:null,
  //   description:"thanhuynh123",
  //   latitude:1313,
  //   longitude:2213,
  //   optionalChoice:"thanhuynh123",
  //   optionalNote:"thanhuynh123"
  // });
  // newEvent.save();

  // Event.getEventByEventId("597874cc4220e04e200f1de3",(err,event)=>{
  //   if (err) throw err;
  //   console.log("eventObj-getEventByEventId ", event );
  //
  //   let newEnrollment = new Enrollment({
  //     event: event._id,
  //     enUserRole: "{AAAAAAA: String,default}",
  //     authCode: "than huynh",
  //     checkIn: "{AAAAAAA: Boolean,default:1}",
  //     checkInDate: "",
  //     confirm: "{AAAAAAA: Boolean,default:}",
  //     enrollDate: "",
  //     emailReminder: "{AAAAAAA: Boolean,default:1}",
  //     optionalNote: "{AAAAAAA: String,default:}",
  //   });
  //   newEnrollment.save();
  //   console.log("post-newEnrollment:::: ", newEnrollment);
  // });
});

//--------------------Create enrollment when a participant join the event-----------------------
router.post('/create',passport.authenticate('jwt', {session:false}), (req, res) => {
  let eventId = req.body.eventId;
  let userRole = req.body.userRole;
  let userId = req.user._id;

  Enrollment.addEnrollment(userId,eventId, userRole, (err,enrollment)=>{
    if (err) throw err;
    console.log("return EN---------", enrollment);
    return res.json({success: true, msg: userRole + " enroll with event " + eventId + " saved",
      createdEnrollment: enrollment});
  });
});
module.exports = router;
