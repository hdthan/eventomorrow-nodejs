const mongoose = require('mongoose');
const relationship = require("mongoose-relationship");
const bcrypt = require('bcryptjs');
const config = require('../../config/database');
const Enrollment = require('./../enrollment/enrollment');
const ObjectId = require("mongoose").Schema.Types.ObjectId;

// User Schema
const UserSchema = new mongoose.Schema({
  _userId: {type: Number},
  email: {type: String, required: true},
  password: {type: String, required: true},
  fullName: {type: String, required: true},
  gender: {type: Boolean, default: 1},
  avatar: { data: Buffer, contentType: String },
  userBirthday: {type: Date},
  phone: {type: String},
  address: {type: String},
  district: {type: String},
  province: {type: String},
  country: {type: String},
  rangeAge: {type: String},
  job: {type: String},
  ignoreJoin: {type: Boolean},
  accessToken: {type: String},
  accountType: {type: String},
  accountCode: {type: String},
  interest: {type: String},
  optionalAllEmailReminder: {type: Boolean, default:1},
  company: {type: String},
  firebaseTk: {type: String},
  errorMessage: {type: String, default:"Error"},
  permission: {type: String, default:"user"},
  enrollments:[{type: ObjectId,ref: "Enrollment"}]
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = (id, callback) => {
  let currentUser = User.findById(id, callback);
  // console.log("currentUser---------------------------",currentUser);

};

module.exports.addUser = (newUser, cb) => {
  console.log("accountCode-addUser "+newUser.accountCode);

  User.getUserByAccountId(newUser.accountCode, (err,user)=>{
    if(user)return cb("This email address was already sign up", null);
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if(err) throw err;
        newUser.password = hash;
        newUser.save(cb);
      });
    });
  })
};

module.exports.updateUserPass = (accId, newPass, cb) => {
  User.findOneAndUpdate(accId, newPass, cb);
};

module.exports.updateUserProfile = (accId, newProfile, cb) => {
  User.findOneAndUpdate(accId, newProfile, cb);
};

module.exports.getUserByAccountId = (accountCode, callback)=> {
  let query = {accountCode: accountCode};
  console.log("accountCode-getUserByUsername " + accountCode);
  User.findOne(query, callback);
};

module.exports.comparePassword = (candidatePassword, hash, callback) => {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

