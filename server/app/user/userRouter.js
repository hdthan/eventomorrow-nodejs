const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../../config/database');
const User = require('./user');
const multer = require('multer');
const fs = require('fs');

// -------------------------------------Register---------------------------------------
router.post('/register', (req, res) => {
  let newUser = new User({
    //only 3 field required when register
    fullName: req.body.fullName,
    email:req.body.accountCode,
    accountCode: req.body.accountCode,
    password: req.body.password
  });
  console.log("newUser obj is "+newUser+". Check respond for adding result");
  User.addUser(newUser, (err, user) => {
    if(err){
      console.log(user);
      res.json({success: false, msg:'Error: ' + err});
      //this err passed from model by mongoose, now send to client
    } else {
      res.json({success: true, msg:'User registered'});
      console.log("user " + user.fullName + " was saved");
    }
  });
});//end Register

// --------------------------------------Authenticate--------------------------------------
router.post('/authenticate', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  User.getUserByAccountId(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found'});
    }
    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign(user, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email
          }
        })
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    });
  });
});//end Auth

module.exports = router;
