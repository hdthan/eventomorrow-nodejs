const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const ObjectId = require("mongoose").Schema.Types.ObjectId;
const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    relationship = require("mongoose-relationship");

router.post('',passport.authenticate('jwt', {session:false}), (req, res) => {
  /** node mongo-rela example
   var ParentSchema = new Schema({
    children: [{type: Schema.ObjectId, ref: "Child"}]
  });
   var Parent = mongoose.model("Parent", ParentSchema);

   var ChildSchema = new Schema({
    parent: {type: Schema.ObjectId, ref: "Parent", childPath: "children"}
  });
   ChildSchema.plugin(relationship, {relationshipPathName: 'parent'});
   var Child = mongoose.model("Child", ChildSchema);

   var parent = new Parent({});
   parent.save();
   var child = new Child({parent: parent._id});
   child.save(); //the parent children property will now contain child's id
   child.remove(); //the parent children property will no longer contain the child's id
   **/
   var ParentSchema = new Schema({
    children: [{type: Schema.ObjectId, ref: "Child"}]
  });
   var Parent = mongoose.model("Parent", ParentSchema);

   var ChildSchema = new Schema({
    parent: {type: Schema.ObjectId, ref: "Parent", childPath: "children"}
  });
   ChildSchema.plugin(relationship, {relationshipPathName: 'parent'});
   var Child = mongoose.model("Child", ChildSchema);

   var parent = new Parent({});
   parent.save();
   var child = new Child({parent: parent._id});


   child.save(); //the parent children property will now contain child's id
   // child.remove(); //the parent children property will no longer contain the child's id


  //
  // var mongoose = require('mongoose')
  //     , Schema = mongoose.Schema;
  //
  // var personSchema = Schema({
  //   _id     : Number,
  //   name    : String,
  //   age     : Number,
  //   stories : [{ type: ObjectId, ref: 'Story' }]
  // });
  //
  // var storySchema = Schema({
  //   _creator : { type: Number, ref: 'Person' },
  //   title    : String,
  //   fans     : [{ type: Number, ref: 'Person' }]
  // });
  //
  // var Story  = mongoose.model('Story', storySchema);
  // var Person = mongoose.model('Person', personSchema);
  //
  // //
  // // var aaron = new Person({ _id:7, name: 'Aaron', age: 100 });
  // //
  // // aaron.save(function (err) {
  // //   if (err) throw err;
  // //
  // //   var story1 = new Story({
  // //     title: "Once upon a timex.7",
  // //     _creator: aaron._id    // assign the _id from the person
  // //   });
  // //
  // //   story1.save(function (err) {
  // //     if (err) throw err;
  // //     // thats it!
  // //   });
  // // });
  //
  // Story.
  // findOne({ title: 'Once upon a timex.' }).
  // populate('_creator').
  // exec(function (err, story) {
  //   if (err) throw err;
  //   console.log('The creator is %s', story._creator.name);
  //   // prints "The creator is Aaron"
  // });

  // Story.
  // findOne({ title: 'Once upon a timex.' }).
  // populate('_creator').
  // exec(function (err, story) {
  //   if (err) return handleError(err);
  //   console.log('The creator is %s', story._creator.name);
  //   // prints "The creator is Aaron"
  // });
});
module.exports = router;
