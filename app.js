const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./server/config/database');
const passport = require('passport');
const fs = require('fs');


let hostname = '127.0.0.1';
let port = process.env.PORT || 8080;

let app = express(); //init app
mongoose.connect(config.database);
let db = mongoose.connection;

//Check Conn
db.once('open', function () {
    console.log('connected to MongoDB at '+ config.database);
});

//Check DB
db.on('error', function (err) {
    console.log(err);
});

//Bring in models
// let Article = require('./models/article');
let users = require('./server/app/user/userRouter');
let ex = require('./server/app/user/ex');
let profile = require('./server/app/profile/profileRouter');
let event = require('./server/app/event/eventRouter');
let enrollment = require('./server/app/enrollment/enrollmentRouter');

// let noti = (req, res, next) => {
//     console.log("this is noti");
//     next();
// };
// app.use(noti); //Middleware
app.use(function(req, res, next) {
  res.on('header', function() {
    console.log('HEADERS GOING TO BE WRITTEN');
  });
  next();
});
app.use(cors());
app.use(function(req,res,next){
  var _send = res.send;
  var sent = false;
  res.send = function(data){
    if(sent) return;
    _send.bind(res)(data);
    sent = true;
  };
  next();
});
//Passport Middleware
app.use(passport.initialize());
app.use(function(req, res, next){
  if(req.url.match('/secure'))
    passport.session()(req, res, next)
  else
    next(); // do not invoke passport
});
require('./server/config/passport')(passport);

//Middleware for JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// //ejs view
// app.set('view engine', 'ejs');
// app.set('views', path.join(__dirname,'client/views'));

//set URL for API in routes
app.use('/users', users);
app.use('/ex', ex);
app.use('/profile', profile);
app.use('/event', event);
app.use('/enrollment', enrollment);

//set static path
app.use(express.static(path.join(__dirname,'public')));

// Index Route
app.get('/', (req, res) => {
    res.send('Invalid Endpoint');
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

// app.get('/', (req, res) => {
//   // res.send('Hello, this is response send form server');
//     // Article.find({}, function (err, articles) {
//     //     if(err){
//     //         console.log(err);
//     //     } else {
//     //         res.render('index', {
//     //             title: 'Article',
//     //             articles: articles
//     //         });
//     //         console.log(articles);}
//     // });
//   // res.render('index'); //render ejs
// }); //This is route handler, file name is index, can pass data as 2nd parameter

app.listen(port,hostname, () => {console.log("server start and listen on port "+port)});
