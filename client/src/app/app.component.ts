import { Component } from '@angular/core';
// import {ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // styleUrls: ['./../style.scss'],
  // encapsulation: ViewEncapsulation.None
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
