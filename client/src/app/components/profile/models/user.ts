export class User {
  accountCode: string;
  email: string;
  password: string;
  fullName: string;
  avatar: string;
  userBirthday: string;
  phone: string;
  address: string;
  district: string;
  province: string;
  country: string;
  rangeAge: string;
  job: string;
  ignoreJoin: string;
  accountType: string;
  interest: string;
  company: string;
  errorMessage: string;
  optionalAllEmailReminder: string;
  gender: string;
}
