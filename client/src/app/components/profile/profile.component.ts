import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import { MyDatePicker } from 'mydatepicker';
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from './models/user';
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { IMyDpOptions  } from 'mydatepicker';
import {FlashMessagesService} from 'angular2-flash-messages';
import {_iterableDiffersFactory} from "@angular/core/src/application_module";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [DatePipe]
})
export class ProfileComponent implements OnInit {
  constructor(private authService:AuthService, private router:Router,
              private http: Http,private datePipe: DatePipe,
              private flashMessage:FlashMessagesService) {}
  private user: User;
  private oppositeSex: String = '';
  private userUrl = "profile/";
  private userBirthdayView: any = { date: { year:2000, month: 1, day: 1 } };

  //set reference
  private profileInfo: object = this;

  //my date picker option
  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd mmm yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    height: '30px', width: '180px',
    inline: false,
    selectorWidth: '220px',
    selectorHeight: '160px',
    showTodayBtn: false,
    disableSince: { year: 2010, month: 1, day: 1}
  };

  ngOnInit() {
    this.authService.getProfile().subscribe(
      profile => {
        this.user = profile.users;
        console.log("result profile:", {"user": this.user});
        if(this.user.userBirthday){
          let userBirthday = new Date(this.user.userBirthday);
          this.userBirthdayView = {
            date: {
              year: userBirthday.getFullYear(),
              month: userBirthday.getMonth(),
              day: userBirthday.getDate()
            }
        }}
        if(this.user.gender === 'true'){this.oppositeSex = ''}else this.oppositeSex = '1';
        // console.log("default userBirthday: ", this.userBirthdayView,"---------userBirthday from db: ", userBirthday);
      },
      err => {
        console.log(err);
        return false;
      }
    );

  }

  changeGender(){
    (this.user.gender==='true') ? this.user.gender = 'false' : this.user.gender = 'true';
    (this.user.gender === 'true') ?this.oppositeSex = '' : this.oppositeSex = '1';
  }

  onSubmit( event: Event) {
    event.preventDefault();
    if ( this.userBirthdayView != null ){
      this.user.userBirthday = this.userBirthdayView.date.year+ "-" + this.userBirthdayView.date.month + "-" + this.userBirthdayView.date.day + "T00:00:00.666Z";
    }
    this.updateProfile(this.user);
  }

  updateProfile(user: User) {
    console.log(user);

    for (let propName in user) {
      delete user[0], user[1], user['password'] , user['_id'];
      if (user[propName] === null || user[propName] === undefined) {
        delete user[propName];
      }
    }
    let userString = JSON.stringify(user);
    console.log(userString);
    this.authService.updateProfile(userString).subscribe(data => {
      if(data.success){
        this.flashMessage.show('Profile was saved!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  updateProfileImg( id: number, img : string){
    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.patch(this.userUrl + `${id}`, img, options)
      .map((res: Response) => {
        console.log(res.statusText);
        return (res.status == 200) ? "true" : 'fail' // return json if status response is Ok
      })
      .catch(this.handleError);

  }


  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
